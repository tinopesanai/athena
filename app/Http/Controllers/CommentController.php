<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use App\Models\Comment;
use App\Models\Application;
use Illuminate\Http\Request;
use App\Models\File;

use Illuminate\Support\Facades\Auth;

class CommentController extends Controller
{

    /**
     * Display relavent comment.
     *
     * */
     public function show(Comment $comment) 
    {

        return view('comments.show', ['comment' => $comment]);
        
    }
    

    public function destroy(Comment $comment)
    {

        $commentToDelete = Comment::findOrFail($comment -> id);

        $applicationID = $comment -> application() -> get()[0] -> id;

        $commentToDelete->delete();

        return redirect()->route('applications.show', ['application' => $applicationID])->with('message', 'Comment was deleted.');

    }

     


/**
     * Display comment creation form.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Application $application)
    {
        $user = Auth::user();

        return view('comments.create', ['application' => $application, 'user' => $user]);
    }


    /**
     * Attempt to save a new comment.
     *
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $validatedData = $request -> validate ([
            'commenter_type' => 'required|string|max:20',
            'stance' => 'required|string|max:7',
            'body' => 'required|string|max:100',
            'user_id' => 'required|integer',
            'application_id' => 'required|integer'
            ]);
    
            $comment = new Comment;
            $comment -> commenter_type = $validatedData['commenter_type'];
            $comment -> stance = $validatedData['stance'];
            $comment -> body = $validatedData['body'];
            $comment -> user_id = $validatedData['user_id'];
            $comment -> application_id = $validatedData['application_id'];
    
            $comment -> save();

            session() -> flash('message', 'Comment Published');

            return redirect() -> route('applications.show', ['application' => $comment -> application_id ]);

    }

}
