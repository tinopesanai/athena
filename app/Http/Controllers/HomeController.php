<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use App\Models\Application;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{



    /**
     * Display homepage.
     *
     * */
    public function index()
    {
        $user = Auth::user();

        if($user != null){
        
            $applications = Application::where('postcode', '=', $user -> postcode)-> simplePaginate(4);
        
        } else {
        
            $applications = DB::table('applications')->simplePaginate(4);
        
        }
        
        $actualApplications = Application::get();
        return view('home.index', ['user' => $user, 'applications' => $applications, 'actualApplications' => $actualApplications]);

    }

}
