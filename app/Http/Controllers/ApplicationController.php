<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use App\Models\Application;
use App\Models\Comment;
use App\Models\File;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

use Illuminate\Support\Facades\DB; //delete after dd


class ApplicationController extends Controller
{
   
    /**
     * Display all applications.
     *
     * */
    public function index() 
    {

        $applications = Application::get();
        $user = Auth::user();

            return view('applications.index', ['applications' => $applications, 'user' => $user]);
        
    }

    /**
     * Display relavent applications with searched reference or postcode.
     *
     * */
    public function search(Request $search) 
    {



        $validatedData = $search -> validate ([
            'wildcard' => 'required|max:25'
            ]);
    
        $newApplication = new Application;
        $newApplication -> reference = $validatedData['wildcard'];
        $newApplication -> alt_reference = "x";
        $newApplication -> date_received = "x";
        $newApplication -> date_validated = "x";
        $newApplication -> full_address = "x";
        $newApplication -> postcode = $validatedData['wildcard'];
        $newApplication -> proposal = "x";
        $newApplication -> status = "x";
        $newApplication -> appeal_status = "x";
        $newApplication -> appeal_decision = "x";

        $applicationIDRef = $this-> getApplicationIDRef($newApplication);

        $application = Application::find($applicationIDRef);

        $user = Auth::user();

        if($application != null){

            return $this -> show($application);

        }
        
        
        $actualApplications = Application::get();
        
        $applications = Application::where('postcode', '=', $newApplication -> postcode)-> simplePaginate(4);

        return view('applications.index', ['applications' => $applications, 'user' => $user, 'actualApplications' => $actualApplications]);
        
    }

    /**
     * Display relavent application, comments and/or report.
     *
     * */
     public function show(Application $application) 
    {
        $user = Auth::user();
        $comments = $application -> comments() -> get();

        $numFlag = 0;

        $for = Comment::where('stance', '=', "for") -> where('application_id', '=', $application -> id) -> count();
        $against = Comment::where('stance', '=', "against") -> where('application_id', '=', $application -> id) -> count();
        
        if($application -> files() -> count() == 1){


            return view('applications.show', ['application' => $application, 'user' => $user, 
            'comments' => $comments, 'numFlag' => $numFlag, 'for' => $for, 'against' => $against]);
        }

        return view('applications.show', ['application' => $application, 'user' => $user, 
            'comments' => $comments, 'numFlag' => $numFlag, 'for' => $for, 'against' => $against]);
        
    }

    
    /**
     * Display application creation form.
     *
     * */
    public function create() 
    {
        $user = Auth::user();

        return view('applications.create', ['user' => $user]);
        
    }

    /**
     *Delete and application.
     *  
     */
    public function destroy(Application $application)
    {

        $applicationToDelete = Application::findOrFail($application -> id);

        $applicationToDelete->delete();

        return redirect()->route('home.index')->with('message', 'Application was deleted.');

    }

    /**
     * Attempt to save a new application.
     *
     * */
    public function store(Request $request) 
    {

        
        $validatedData = $request -> validate ([
            
            'reference' => 'required|string|max:20',
            //'file' => 'nullable|mimes:jpeg,png|max:1014',
            'alt_reference' => 'required|string|max:20',
            'date_received' => 'required|string|max:20',
            'date_validated' => 'required|string|max:20',
            'full_address' => 'required|string|max:20',
            'postcode' => 'required|string|max:20',
            'proposal' => 'required|string|max:100',
            'status' => 'required|string|max:20',
            'appeal_status' => 'required|string|max:20',
            'appeal_decision' => 'required|string|max:20',
            ]);

        $application = new Application;
      
        $application -> reference = $validatedData['reference'];
        $application -> alt_reference = $validatedData['alt_reference'];
        $application -> date_received = $validatedData['date_received'];
        $application -> full_address = $validatedData['full_address'];
        $application -> postcode = $validatedData['postcode'];
        $application -> proposal = $validatedData['proposal'];
        $application -> status = $validatedData['status'];
        $application -> appeal_status = $validatedData['appeal_status'];
        $application -> appeal_decision = $validatedData['appeal_decision'];
        $application -> date_validated = $validatedData['date_validated'];

        $application -> save();
        
        session() -> flash('message', 'Application Published');
                
        return redirect() -> route('home.index');
       
    }
    

    /**
     * Gets the ID of application with matching reference. 
     * If it doesn't exist, null is returned.
     *
     * */
    private function getApplicationIDRef(Application $newApplication) 
    {
        $applications = Application::get();

        foreach($applications as $application){
            
            if($application -> reference === $newApplication -> reference){
                

                return $application -> id;

            }

        
        }

        return null;

    }

}
