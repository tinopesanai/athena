<?php

namespace App\Actions\Fortify;

use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Laravel\Fortify\Contracts\CreatesNewUsers;
use App\Models\Profile;

class CreateNewUser implements CreatesNewUsers
{
    use PasswordValidationRules;

    /**
     * Validate and create a newly registered user.
     *
     * @param  array  $input
     * @return \App\Models\User
     */
    public function create(array $input)
    {

        Validator::make($input, [

            'title' => ['required', 'string', 'max:5'],
            'name' => ['required', 'string', 'max:255'],
            'surname' => ['required', 'string', 'max:255'],
            'address_1' => ['required', 'string', 'max:255'],

            'address_2' => ['nullable', 'string', 'max:255'],

            'town' => ['required', 'string', 'max:255'],
            'postcode' => ['required', 'string', 'max:255'],

            'telephone' => ['nullable', 'string', 'max:20'],

            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => $this->passwordRules(),

            'adminCode' => ['nullable', 'string', 'max:10'],

        ])->validate();

        $newUser = new User;

        $newUser -> title = $input['title'];
        $newUser ->  name =  $input['name'];
        $newUser ->  surname =  $input['surname'];
        $newUser -> address_1 = $input['address_1'];
        $newUser ->  address_2 =  $input['address_2'];
        $newUser -> town = $input['town'];
        $newUser ->  postcode =  $input['postcode'];
        $newUser -> telephone = $input['telephone'];
        $newUser -> password = Hash::make($input['password']);
        $newUser -> email = $input['email'];

        if ($input['adminCode'] ===  "admin2020"){
            $newUser -> isAdmin = TRUE;
        } else {
            $newUser -> isAdmin = FALSE;
        }

        $newUser -> save();

        return $newUser;

    }
}
