<?php

use Illuminate\Support\Facades\Route;
use App\Models\Application;
use App\Http\Controllers\ApplicationController;
use App\Models\Comment;
use App\Http\Controllers\CommentController;
use App\Http\Controllers\HomeController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function () {

    return view('welcome');

});


Route::middleware(['auth:sanctum', 'verified'])->get('/home', function () {
    return view('home.index');
})->name('home.index');


Route::get('/home', [HomeController::class, 'index'])
-> name('home.index') -> middleware('auth');

Route::get('/visit', [HomeController::class, 'index'])
-> name('visit.index');

Route::get('/applications/search', [ApplicationController::class, 'search'])
-> name('applications.search') -> middleware('auth');

Route::get('/visit/search', [ApplicationController::class, 'search'])
-> name('visit.search');

Route::get('/applications/create', [ApplicationController::class, 'create'])
-> name('applications.create') -> middleware('auth');

Route::post('/applications/store', [ApplicationController::class, 'store'])
-> name('applications.store') -> middleware('auth');

Route::get('/applications/{application}/edit', [ApplicationController::class, 'edit'])
-> name('applications.edit') -> middleware('auth');

Route::delete('/applications/{application}', [ApplicationController::class, 'destroy'])
-> name('applications.destroy') -> middleware('auth');

Route::put('/applications/{application}', [ApplicationController::class, 'update'])
-> name('applications.update') -> middleware('auth');

Route::get('/applications/{application}', [ApplicationController::class, 'show'])
-> name('applications.show') -> middleware('auth');

Route::get('/visit/{application}', [ApplicationController::class, 'show'])
-> name('visit.show');

Route::post('/comments/store', [CommentController::class, 'store'])
-> name('comments.store') -> middleware('auth');

Route::get('/comments/search', [CommentController::class, 'search'])
-> name('comments.search') -> middleware('auth');

Route::get('/comments/analyse', [CommentController::class, 'analyse'])
-> name('comments.analyse') -> middleware('auth');

Route::get('/comments/create/{application}', [CommentController::class, 'create'])
-> name('comments.create') -> middleware('auth');

Route::get('/comments/{comment}/edit', [CommentController::class, 'edit'])
-> name('comments.edit') -> middleware('auth');

Route::delete('/comments/{comment}', [CommentController::class, 'destroy'])
-> name('comments.destroy') -> middleware('auth');

Route::put('/comments/{comment}', [CommentController::class, 'update'])
-> name('comments.update') -> middleware('auth');

Route::get('/comments/{comment}', [CommentController::class, 'show'])
-> name('comments.show') -> middleware('auth');
