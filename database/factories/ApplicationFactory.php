<?php

namespace Database\Factories;

use App\Models\Application;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class ApplicationFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Application::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
            'reference' => $this -> faker -> shuffle('swanseauni2018-2021'),
            'alt_reference' => $this -> faker -> shuffle('swanseauni2018-2021'),
            'date_received' => $this -> faker -> shuffle('swanseauni2018-2021'),
            'date_validated' => $this -> faker -> shuffle('swanseauni2018-2021'),
            'full_address' => $this -> faker -> streetAddress,
            'postcode' => $this -> faker -> postcode,
            'proposal' => $this -> faker -> text($maxNbChars = 200),
            'status' => $this -> faker -> text($maxNbChars = 20),
            'appeal_status' => $this -> faker -> text($maxNbChars = 20),
            'appeal_decision' => $this -> faker -> text($maxNbChars = 20),
        ];
    }
}
