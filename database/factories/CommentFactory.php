<?php

namespace Database\Factories;

use App\Models\Comment;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class CommentFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = \App\Models\Comment::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //

            'commenter_type' => $this -> faker -> word(),
            'stance' => $this -> faker -> randomElement($array = array('for','against')),
            'body' => $this -> faker -> text($maxNbChars = 100),

            'user_id'=> \App\Models\User::inRandomOrder()->first()->id,
            'application_id'=> \App\Models\Application::inRandomOrder()->first()->id,


        ];
    }
}
