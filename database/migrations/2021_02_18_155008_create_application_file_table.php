<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateApplicationFileTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('application_file', function (Blueprint $table) {

            $table->primary(['application_id', 'file_id']);

            
            $table->bigInteger('application_id')->unsigned();
            $table->bigInteger('file_id')->unsigned();

            $table->string('name');
            $table->string('url');

            $table->foreign('application_id')->references('id')->on('applications')
            ->onDelete('cascade')->onUpdate('cascade');

            $table->foreign('file_id')->references('id')->on('files')
            ->onDelete('cascade')->onUpdate('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('application_file');
    }
}
