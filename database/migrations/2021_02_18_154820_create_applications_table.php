<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateApplicationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('applications', function (Blueprint $table) {

            $table->id('id');
            $table->timestamps();

            $table->string('reference');
            $table->string('alt_reference');
            $table->string('date_received');
            $table->string('date_validated');
            $table->string('full_address');
            $table->string('postcode');

            $table->text('proposal');

            $table->string('status');
            $table->string('appeal_status');
            $table->string('appeal_decision');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('applications');
    }
}
