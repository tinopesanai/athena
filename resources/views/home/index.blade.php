<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
         Home

        </h2> 

    </x-slot>
    @if($user != null)
    
    <div class="py-12">
    <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
    
    <div class="mt-8 font-bold text-xl text-gray-800 leading-tight flex items-center">
        Hello {{ $user -> name }}!
    </div>
    
    <div>
    <div>
    <form method = "GET" action = "{{ route('applications.search') }}">

        @csrf
        <div>
        <div class="mt-8 font-bold text-xl text-gray-800 leading-tight flex items-center">
        Search Applications By Postcode or Reference
        </div>
        <x-jet-input id="wildcard" type="text" name="wildcard" />
        </div>
    </div>

            <button type="submit" class="inline-flex items-center px-4 py-2 bg-gray-800 border border-transparent rounded-md font-semibold text-xs text-white uppercase tracking-widest hover:bg-gray-700 active:bg-gray-900 focus:outline-none focus:border-gray-900 focus:shadow-outline-gray disabled:opacity-25 transition ease-in-out duration-150" wire:loading.attr="disabled" wire:target="photo">
            Search
            </button>
    </form>
    </div>
    
    </div>

   

    @if($user -> isAdmin == FALSE )
    <div class="mt-8 ml-4 font-bold text-xl text-gray-800 leading-tight flex items-center">Here are all the Applications near you:</div>
        <div class="mt-8 ml-4 bg-white dark:bg-gray-800 overflow-hidden shadow sm:rounded-lg">
        <div class="grid grid-cols-1 md:grid-cols-2">
      
            @foreach ($applications as $application)
            
            <ul>
                <div class="p-6">
                    <div class="flex items-center">
                        <div class=" text-lg leading-7 font-semibold">
                            <a href=" {{ route('applications.show', ['application' => $application -> id ]) }}" class="underline text-gray-900 dark:text-white"> {{$application -> reference}}</a>
                        </div>
                    </div>
                            
                        
                </div>
            </ul>
            
            @endforeach
        </div>
        @if($applications -> count() == null)
            Nothing here. Try searching around your area.
        @endif
        </div>
        
       <div>
       {{ $applications->links() }}
      
       </div>
    </div>
    @endif
    </div>
    @else
    <div class="py-12">
    <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">

    <div class="p-6">
    <div class="flex items-center">
        Hello Visitor!
    </div>
    </div>

    <div>
    <div>
    <form method = "GET" action = "{{ route('visit.search') }}">

        @csrf
        <div>
        <div class="mt-8 font-bold text-xl text-gray-800 leading-tight flex items-center">
        Search Applications By Postcode or Reference
        </div>
        <x-jet-input id="wildcard" type="text" name="wildcard" />
        </div>
    </div>

            <button type="submit" class="inline-flex items-center px-4 py-2 bg-gray-800 border border-transparent rounded-md font-semibold text-xs text-white uppercase tracking-widest hover:bg-gray-700 active:bg-gray-900 focus:outline-none focus:border-gray-900 focus:shadow-outline-gray disabled:opacity-25 transition ease-in-out duration-150" wire:loading.attr="disabled" wire:target="photo">
            Search
            </button>
    </form>
    </div>
    
    </div>
    <div class="mt-8 ml-4 font-bold text-xl text-gray-800 leading-tight flex items-center">Here are all the Applications:</div>
        <div class="mt-8 bg-white dark:bg-gray-800 overflow-hidden shadow sm:rounded-lg">
        <div class="grid grid-cols-1 md:grid-cols-2">

            @foreach ($applications as $application)
            <ul>
                <div class="p-6">
                    <div class="flex items-center">
                        <div class="ml-4 text-lg leading-7 font-semibold">
                            <a href=" {{ route('visit.show', ['application' => $application -> id ]) }}" class="underline text-gray-900 dark:text-white">{{$application -> reference}}</a>
                        </div>
                    </div>
                </div>
            </ul>
            @endforeach
        </div>
        </div>
        {{ $applications->links() }}
        

    </div>
    </div>
    @endif
</x-app-layout>
