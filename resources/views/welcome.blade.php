<x-guest-layout>
    <x-jet-authentication-card>
        <x-slot name="logo">
            <x-jet-authentication-card-logo />
        </x-slot>

        <x-jet-validation-errors class="mb-4" />
    <body class="antialiased">
    <div class="relative flex items-top justify-center"><p>Welcome to Athena</p></div>
        <div class="relative flex items-top justify-center">

            @if (Route::has('login'))
                <div>
                    @auth
                    <x-jet-button class="ml-4">
                    <a href = "{{ route('home.index') }}"> Home </a>
                    </x-jet-button>
                    @else
                        <x-jet-button class="ml-4">
                        <a href = "{{ route('login') }}"> Login </a>
                        </x-jet-button>
                        <x-jet-button class="ml-4">
                        <a href = "{{ __('visit') }}"> Visit </a>
                        </x-jet-button>
                        @if (Route::has('register'))
                        <x-jet-button class="ml-4">
                        <a href = "{{ route('register') }}"> Register </a>
                        </x-jet-button>
                        @endif
                    @endif
                </div>
            @endif
        </div>
    </body>
    </x-jet-authentication-card>
</x-guest-layout>
