<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
               
               Create Application 
           
        </h2>
    </x-slot>
    <form class="m-2" method="post" action="{{ route('applications.store') }}" enctype="multipart/form-data">

    @csrf
    <div>
        
        <div>

        <x-jet-label for="reference" value="Reference" />
  
    
            <div>
            <x-jet-input id="reference" type="text" name="reference" />
            </div>
        </div>

<div>
            <x-jet-label for="alt_reference" value="Alt Reference" />            
            <x-jet-input id="alt_reference" type="text" name="alt_reference" />
     
            <x-jet-label for="date_received" value="Date Received" />
            <x-jet-input id="date_received" type="text" name="date_received" />

            <x-jet-label for="date_validated" value="Date Validated" />
            <x-jet-input id="date_validated" type="text" name="date_validated" />

            <x-jet-label for="full_address" value="Full Address" />
            <x-jet-input id="full_address" type="text" name="full_address" />
     
            <x-jet-label for="postcode" value="Postcode" />
            <x-jet-input id="postcode" type="text" name="postcode" />

</div>
        <div>
        <div>
        <x-jet-label for="proposal" value="Proposal" />
        </div>
            <div>
            <textarea name="proposal" rows="10" cols="100"></textarea>
            </div>
        </div>

            <x-jet-label for="status" value="Status" />
            <x-jet-input id="status" type="text" name="status" />

            <x-jet-label for="appeal_status" value="Appeal Status" />
            <x-jet-input id="appeal_status" type="text" name="appeal_status" />

            <x-jet-label for="appeal_decision" value="Appeal Decision" />
            <x-jet-input id="appeal_decision" type="text" name="appeal_decision" />

        <div>
        
        <button type="submit" class="inline-flex items-center px-4 py-2 bg-gray-800 border border-transparent rounded-md font-semibold text-xs text-white uppercase tracking-widest hover:bg-gray-700 active:bg-gray-900 focus:outline-none focus:border-gray-900 focus:shadow-outline-gray disabled:opacity-25 transition ease-in-out duration-150" wire:loading.attr="disabled" wire:target="photo">
        Publish
        </button>

        <x-jet-button class="ml-4">
        <a href = "{{ route('home.index') }}"> Cancel
        </x-jet-button>
        
        </div>

    </div>
</form>

</x-app-layout>