<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
               
               Showcasing application 
           
        </h2>
    </x-slot>
                    
    
@if($user != null)
    @if($user -> isAdmin)   
    
   <form method="post" action=" {{ route('applications.destroy', ['application' => $application]) }} ">
                        
        @csrf
        @method('DELETE')

        <button type="submit" class="ml-4 mt-8 inline-flex items-center px-4 py-2 bg-gray-800 border border-transparent rounded-md font-semibold text-xs text-white uppercase tracking-widest hover:bg-gray-700 active:bg-gray-900 focus:outline-none focus:border-gray-900 focus:shadow-outline-gray disabled:opacity-25 transition ease-in-out duration-150" wire:loading.attr="disabled" wire:target="photo">
            Delete application
        </button>

    </form>
    @endif

 <div >
                
    <div class="ml-4 mt-8 font-bold text-xl text-gray-800 leading-tight">
       <div>Reference: {{$application -> reference}} </div>
       <div>Postcode: {{$application -> postcode}} </div>
       <div>Status: {{$application -> status}} </div>
    </div>
    
    <div>
        
        <div class="ml-4 mt-8 font-bold text-xl text-gray-800 leading-tight">
        Body:  
        </div>

        <div class="ml-4 bg-white dark:bg-gray-800 overflow-hidden shadow sm:rounded-lg">
        {{$application -> proposal}} 
        </div>
    
    </div>

</div>
   
<div>
    <div class="ml-4 mt-8 font-bold text-xl text-gray-800 leading-tight">

        @if($user -> isAdmin)   
            
                    <div>

                    
                        <div class="ml-4 mt-8 font-bold text-xl text-gray-800 leading-tight">
                            <div>Report: </div>
                            <div>Residents For Application: {{$for}} </div>
                            <div>Residents Against Application: {{$against}}  </div>
                        </div>
                        Comments:
                    </div>
                   
        @endif     

        @foreach ($comments as $comment)
        @if($user -> isAdmin or $comment -> user() -> get()[0] == $user )
            @if($comment -> user() -> get()[0] == $user)
                Your Comment
                @endif
        <?php $numFlag = 1; ?>

            <div> 
            <div class="ml-4 mt-8 bg-white dark:bg-gray-800 overflow-hidden shadow sm:rounded-lg">
           
            <div class="font-bold text-xl text-gray-800 leading-tight flex items-center"> {{ $comment -> user() -> get()[0] -> name }}:</div> 
            {{ $comment -> body }} 

                    <button>
                    <form method="post" action=" {{ route('comments.destroy', ['comment' => $comment]) }} ">
                        
                        @csrf
                        @method('DELETE')

                        <button type="submit" class="ml-4 mt-8 inline-flex items-center px-4 py-2 bg-gray-800 border border-transparent rounded-md font-semibold text-xs text-white uppercase tracking-widest hover:bg-gray-700 active:bg-gray-900 focus:outline-none focus:border-gray-900 focus:shadow-outline-gray disabled:opacity-25 transition ease-in-out duration-150" wire:loading.attr="disabled" wire:target="photo">
                        Delete Comment
                        </button>

                    </form>
                    </button>
    
            </div> 
            </div>
            @endif
           
     @endforeach

     @if($user -> isAdmin == FALSE and $numFlag == 0)   
                <div>
    
                    <x-jet-button class="mt-8 ml-4">
                        <a href = "{{ route('comments.create', ['application' => $application]) }}"> Comment </a>
                    </x-jet-button>
                
                </div>
                
            @endif
     </div>
</div>
    
@else

<div >
<div class="ml-4 mt-8 font-bold text-xl text-gray-800 leading-tight">
                <div>Reference: {{$application -> reference}} </div>
                <div>Postcode: {{$application -> postcode}} </div>
                <div>Status: {{$application -> status}} </div>
                </div>
                
                <div>
                    
                    <div class="ml-4 mt-8 font-bold text-xl text-gray-800 leading-tight">
                    Body:  
                    </div>
            
                    <div class="ml-4 bg-white dark:bg-gray-800 overflow-hidden shadow sm:rounded-lg">
                    {{$application -> proposal}} 
                    </div>
                
                </div>
            
            </div>
               
            
            
                </div>
                @endif

</x-app-layout>