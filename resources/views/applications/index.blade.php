<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
         Applications

        </h2> 

    </x-slot>

    
    <div class="py-12">
    <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">

    <div class="p-6">
    <div class="flex items-center">
    @if($user != null)
        Hello {{ $user -> name }}!
        @else
        Hello Visitor!
        @endif
    </div>
    </div>

        <div class="mt-8 bg-white dark:bg-gray-800 overflow-hidden shadow sm:rounded-lg">
        <div class="grid grid-cols-1 md:grid-cols-2">

            @foreach ($applications as $application)
            <ul>
                <div class="p-6">
                    <div class="flex items-center">
                        <div class="ml-4 text-lg leading-7 font-semibold">
                            <a href=" {{ route('applications.show', ['application' => $application -> id ]) }}" class="underline text-gray-900 dark:text-white">{{$application -> reference}}</a>
                        </div>
                    </div>
                </div>
            </ul>
            @endforeach
        </div>
        </div>

    </div>
    </div>
</x-app-layout>
