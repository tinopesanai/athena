<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
               
               Create Comment 
           
        </h2>
    </x-slot>

<form method = "post" action = "{{ route('comments.store') }}">

    @csrf

    <div>

        <div>
        <div>
        <x-jet-label for="commenter_type" value="Commenter Type" />
        </div>
            <div>
            <textarea name="commenter_type" rows="1" cols="20"></textarea>
            </div>
        </div>

        <div>
        <x-jet-label for="stance" value="For or Against?" />
        </div>
            <div>
            <textarea name="stance" rows="1" cols="20"></textarea>
            </div>
        </div>

        <div>
        <x-jet-label for="body" value="Body" />
        </div>
            <div>
            <textarea name="body" rows="10" cols="30"></textarea>
            </div>
        
        <div>



        <input type = "hidden" name = "application_id" value = "{{ $application -> id}} ">
        <input type = "hidden" name = "user_id" value = "{{ $user -> id }}">

        <button type="submit" class="inline-flex items-center px-4 py-2 bg-gray-800 border border-transparent rounded-md font-semibold text-xs text-white uppercase tracking-widest hover:bg-gray-700 active:bg-gray-900 focus:outline-none focus:border-gray-900 focus:shadow-outline-gray disabled:opacity-25 transition ease-in-out duration-150" wire:loading.attr="disabled" wire:target="photo">
        Comment
        </button>

        <x-jet-button class="ml-4">
        <a href = "{{ route('applications.show', ['application' => $application -> id ]) }}"> Cancel
        </x-jet-button>

        </div>
        </div>
        

    </div>

</form>

</x-app-layout>